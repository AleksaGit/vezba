import React from 'react';
import '../css/footer.css';

const Footer = () =>{
    return(
        <div className="footer">
            <div className="footerCell"><a href="https://www.facebook.com"><img className="siteLogo" src="/slike/facebook.png" alt=""/></a></div>
            <div className="footerCell"><a href="https://www.instagram.com"><img className="siteLogo" src="/slike/instagram.png" alt=""/></a></div>
            <div className="footerCell"><a href="https://twitter.com"><img className="siteLogo" src="/slike/twitter.png" alt=""/></a></div>

        </div>
    )
}

export default Footer;