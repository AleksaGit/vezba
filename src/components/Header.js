import React from 'react';
import {Link} from 'react-router-dom';
import '../css/Header.css';

const Header = () => {
    return (
        <div className="container">
            <div className="box-1"><Link to="/"><img className='logo' src="/slike/logo.png" alt=""/></Link></div>
            <div className="box-2"><Link to="/gallery"><button className="dugme">Gallery</button></Link></div>
            <div className="box-3"><Link to="/list"><button className="dugme">Recipie</button></Link></div>
        </div>
    )
}

export default Header;