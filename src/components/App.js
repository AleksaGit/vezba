import React from 'react';
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import Gallery from './Gallery';
import Home from './Home';
import List from './List';
import NotFound from './NotFound';

const App = () => {
    return (
        <div>
        <BrowserRouter>
            <div>
                <Header/>
                <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/gallery" exact component={Gallery} />
                <Route path="/List" exact component={List} />
                <Route component={NotFound}/>
                </Switch>
            </div>
        </BrowserRouter>
        <Footer/>
        </div>
    )
}

export default App;