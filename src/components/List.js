import React,{useEffect,useState} from 'react';
import Recipe from "./Recipe";
import '../css/list.css';
const List = () => {

    const APP_ID = '4b237e7a';
    const APP_KEY = 'bfa342a494adeb5236b3331565e71653';

    const [recipes,setRecipies] = useState([]);
    const [search,setSearch] = useState('');
    const [querry,setQuerry] = useState('chicken');

    useEffect(() => {
        getRecipes();
    },[querry]);

    const getRecipes = async () => {
        const response = await fetch(`https://api.edamam.com/search?q=${querry}&app_id=${APP_ID}&app_key=${APP_KEY}`)
        const data = await response.json();
        setRecipies(data.hits);
    }

    const updateSearch = e => {
        setSearch(e.target.value);
    }

    const getSearch = e => {
        e.preventDefault();
        setQuerry(search);
        setSearch("");
    }


    return (
        <div>
            <form className="search-form"onSubmit={getSearch}>
                <input className="search-bar" type="text" value={search} onChange = {updateSearch} />
                <button className="search-button" type="submit">Search</button>
            </form>
            <div className="recipes">
            {recipes.map(recipe =>(
              <Recipe key={recipe.recipe.label} title={recipe.recipe.label} calories={recipe.recipe.calories}
              image={recipe.recipe.image} ingredients={recipe.recipe.ingredients}/>  
            ))}
            </div>
        </div>
    )
}

export default List;