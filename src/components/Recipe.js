import React from 'react';
import '../css/recipe.css';

const Recipe = ({title,calories,image,ingredients}) =>{
    return(
        <div className="recipe">
            <h1>{title}</h1>
            <ol>
                {ingredients.map(ingridient =>(
                    <li>{ingridient.text}</li>
                ) )}
            </ol>
            <img src={image} alt=""/>
        </div>
    )
}

export default Recipe;