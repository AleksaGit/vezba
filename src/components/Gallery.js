import React from 'react';
import '../css/gallery.css';

const Gallery = () => {
    return (
        <div className="gallery">
            <div><img src="/slike/1.jpg" alt=""/></div>
            <div><img src="/slike/2.jpg" alt=""/></div>
            <div><img src="/slike/3.jpg" alt=""/></div>
            <div><img src="/slike/4.jpg" alt=""/></div>
            <div><img src="/slike/5.jpg" alt=""/></div>
            <div><img src="/slike/1.jpg" alt=""/></div>
            <div><img src="/slike/2.jpg" alt=""/></div>
            <div><img src="/slike/3.jpg" alt=""/></div>
            <div><img src="/slike/4.jpg" alt=""/></div>
            <div><img src="/slike/5.jpg" alt=""/></div>
        </div>
    )
}

export default Gallery;